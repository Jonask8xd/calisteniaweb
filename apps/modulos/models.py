from django.db import models

# Create your models here.

class Usuario(models.Model):
    username = models.CharField(max_length=4)
    nombre = models.CharField(max_length=30)
    email = models.EmailField()
    password = models.CharField(max_length=12)
    fechaNacimiento = models.DateField()
    puntaje = models.IntegerField()

