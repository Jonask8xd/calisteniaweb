from django.urls import path
from .views import index,registro,login_view,perfil,ejercicios, triceps,espalda,piernas, cerrar_sesion,cambiar_contraseña
from django.conf.urls.static import static



urlpatterns = [
    path('', index,name='index'),
    path('login/',login_view, name='login'),
    path('registro/',registro, name='registro'),
    path('profile/',perfil, name='profile'),
    path('ejercicios/',ejercicios, name='ejercicios'),
    path('triceps/',triceps,name='pechoTriceps'),
    path('espalda/',espalda,name='espaldaBiceps'),
    path('piernas/', piernas,name='piernas'),
    path('cerrar_sesion',cerrar_sesion,name="cerrar_sesion"),
    path('profile/cambiar_clave/',cambiar_contraseña,name='cambiar_clave')
]