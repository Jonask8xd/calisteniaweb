from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login

from django.contrib.auth.decorators import login_required
from django.db.utils import IntegrityError

# redireccionar por nombre de url
from django.urls import reverse

#modelos
from django.contrib.auth.models import User


# Create your views here.
def index(request):#vista para el inicio de la página
    return render(request,"modulos/index.html")

def cerrar_sesion(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def login_view(request):#vista para el inicio de sesión de la página
    if request.method=='POST':
        username = request.POST['usuarioLogin']
        password = request.POST['contraseñaLogin']
        
        user=authenticate(request,username=username,password=password)
        
        if user is not None:
            auth_login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:    
            return render(request,'modulos/login.html',{'error':'Usuario o contraseña inconrrecto'})
    
    return render(request,"modulos/login.html")


def registro(request):#vista para el registro de la página
    if request.method =='POST':
        username = request.POST['username']
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        email = request.POST['email']
        contraseña = request.POST['contraseña']
        contraseña2 = request.POST['contraseña2']

        if contraseña != contraseña2:
            return render(request,'modulos/registro.html',{'error':'Las contraseñas deben coincidir'})
        
        try:
            userNew = User.objects.create_user(username=username,password=contraseña)
        except IntegrityError:
            return render(request, 'modulos/registro.html', {'error': 'este usuario ya se encuentra registrado'})
        userNew.first_name = nombre
        userNew.last_name = apellido
        userNew.email = email
        userNew.save()

        return redirect('login')



    return render(request,"modulos/registro.html")

@login_required(login_url='login')
def perfil(request):
    if request.POST:
        first_name= request.POST.get('nombre',False)
        last_name= request.POST.get('apellido',False)
        username= request.POST.get('username',False)
        email=request.POST.get('email',False)
        if first_name and last_name:
            request.user.first_name = first_name
            request.user.last_name = last_name
            request.user.email = email
            request.user.save()
        else:
            pass
    usuario = request.user
    contexto = {'usuario':usuario}
    return render(request,"modulos/profile.html",contexto)


@login_required(login_url = 'login')
def cambiar_contraseña(request):
    if request.POST:
        password = request.POST.get('contraseña',False)
        re_password = request.POST.get('contraseña2',False)
        if password == re_password:
            request.user.set_password(password)
            request.user.save()
            logout(request)
    return HttpResponseRedirect(reverse('login'))


def ejercicios(request):
    return render(request,"modulos/ejercicios.html")
    
def triceps(request):
    return render(request,"modulos/Ejercicios/triceps.html")


def espalda(request):
    return render(request,"modulos/Ejercicios/espalda.html")

def piernas(request):
    return render(request,"modulos/Ejercicios/piernas.html")